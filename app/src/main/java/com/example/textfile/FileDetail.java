package com.example.textfile;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileDetail extends AppCompatActivity {

    EditText title, content;
    Button save, delete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_detail);

        title = (EditText)findViewById(R.id.title);
        content = (EditText)findViewById(R.id.content);
        save = (Button)findViewById(R.id.button_save);
        delete = (Button)findViewById(R.id.button_delete);
        final Intent intent = getIntent();
        final String selectedFileName = intent.getStringExtra("selectedFileName");
        String[] files = intent.getStringArrayExtra("files");
        if(selectedFileName.equals("Create A New File")){
            delete.setVisibility(View.INVISIBLE);
            title.setHint("Input File Name Here");
            content.setHint("Write Something Here...");
        }
        else{
            title.setText(selectedFileName);
            title.setKeyListener(null);
            try {
                content.setText(readFromFile(selectedFileName));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(title.getText().toString().length()==0){
                    Toast.makeText(FileDetail.this,"File name shouldn't be null",Toast.LENGTH_SHORT).show();
                }
                else if(content.getText().toString().length()==0){
                    Toast.makeText(FileDetail.this,"Please write something",Toast.LENGTH_SHORT).show();
                }
                else {
                    try {
                        writeToFile(title.getText().toString(), content.getText().toString());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Toast.makeText(FileDetail.this, "Saved!", Toast.LENGTH_SHORT).show();
                    Intent intent_back = new Intent(FileDetail.this, MainActivity.class);
                    startActivity(intent_back);
                }
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                File file = new File(getFilesDir(),selectedFileName);
                file.delete();
                Toast.makeText(FileDetail.this,"Deleted!",Toast.LENGTH_SHORT).show();
                Intent intent_back = new Intent(FileDetail.this, MainActivity.class);
                startActivity(intent_back);
            }
        });
    }
    public String readFromFile(String fileName) throws IOException {
        String fileContent="";
        try{
            FileInputStream fileInputStream = openFileInput(fileName);
            byte[] buffer = new byte[fileInputStream.available()];
            fileInputStream.read(buffer);
            fileContent=new String(buffer,"UTF-8");
            fileInputStream.close();
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return fileContent;
    }

    public void writeToFile(String fileName, String fileContent) throws IOException {
        try{
            FileOutputStream fileOutputStream =openFileOutput(fileName, MODE_PRIVATE);
            fileOutputStream.write(fileContent.getBytes());
            fileOutputStream.close();
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
}