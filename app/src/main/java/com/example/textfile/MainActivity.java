package com.example.textfile;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    ListView lv1;
    String[] files;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        try {
            defaultFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        files= this.fileList();
        lv1 = (ListView) findViewById(R.id.listview1);
        final List<String> fileNames = new ArrayList<String>();
        fileNames.add("Create A New File");
        for(String s : files){
            fileNames.add(s);
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, fileNames);
        lv1.setAdapter(adapter);
        lv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String selectedFileName = fileNames.get(i);
                Intent intent = new Intent(MainActivity.this, FileDetail.class);
                intent.putExtra("selectedFileName",selectedFileName);
                intent.putExtra("files", files);
                startActivity(intent);
            }
        });
    }

    public void defaultFile() throws IOException {
        File file = new File(getFilesDir(),"default.txt");
        if(!file.exists()){
            System.out.println("aaa");
            writeToFile("default.txt", "Gain more experience and improve skills." +
                    "\n\n"+"This .txt file is the default and can only be modified. If trying to delete it," +
                    " the content will change back to the default.");
        }
    }

    public void writeToFile(String fileName, String fileContent) throws IOException {
        try{
            FileOutputStream fileOutputStream =openFileOutput(fileName, MODE_PRIVATE);
            fileOutputStream.write(fileContent.getBytes());
            fileOutputStream.close();
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
}